/*
package com.rsc.bricks.View;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;

import com.rsc.bricks.R;

import java.util.ArrayList;


*/
/**
 * Created by Rudy Schoepfer on 13/03/2017.
 *//*


public class Next_View_Surface extends View{

    Paint color1transppaint,color2transppaint, color3transppaint;
    Paint color1redpaint,color2redpaint, color3redpaint;
    Paint color1bluelightpaint,color2bluelightpaint, color3bluelightpaint;
    Paint color1bluepaint,color2bluepaint, color3bluepaint;
    Paint color1yellowpaint,color2yellowpaint, color3yellowpaint;
    Paint color1pinkpaint,color2pinkpaint, color3pinkpaint;
    Paint color1greenpaint,color2greenpaint, color3greenpaint;
    Paint color1brownpaint,color2brownpaint, color3brownpaint;


    Path path = new Path();
    Canvas canvas;
    private int[][] mGame;
    int numWidth,numHeight;

    public Next_View_Surface(Context context) {
        super(context);
        init(context,null);
    }
    public Next_View_Surface(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }
    public Next_View_Surface(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);
    }
    private void init(Context context,AttributeSet attrs){
        if(context!=null && attrs!= null)
        {
            //initialisation des pinceaux et des couleurs
            color1transppaint=new Paint();
            color1transppaint.setColor(getResources().getColor(R.color.transpcolor1));
            color2transppaint=new Paint();
            color2transppaint.setColor(getResources().getColor(R.color.transpcolor2));
            color3transppaint=new Paint();
            color3transppaint.setColor(getResources().getColor(R.color.transpcolor3));
            color1redpaint=new Paint();
            color1redpaint.setColor(getResources().getColor(R.color.redcolor1));
            color2redpaint=new Paint();
            color2redpaint.setColor(getResources().getColor(R.color.redcolor2));
            color3redpaint=new Paint();
            color3redpaint.setColor(getResources().getColor(R.color.redcolor3));
            color1bluelightpaint=new Paint();
            color1bluelightpaint.setColor(getResources().getColor(R.color.bluelightcolor1));
            color2bluelightpaint=new Paint();
            color2bluelightpaint.setColor(getResources().getColor(R.color.bluelightcolor2));
            color3bluelightpaint=new Paint();
            color3bluelightpaint.setColor(getResources().getColor(R.color.bluelightcolor3));
            color1bluepaint=new Paint();
            color1bluepaint.setColor(getResources().getColor(R.color.bluecolor1));
            color2bluepaint=new Paint();
            color2bluepaint.setColor(getResources().getColor(R.color.bluecolor2));
            color3bluepaint=new Paint();
            color3bluepaint.setColor(getResources().getColor(R.color.bluecolor3));
            color1yellowpaint=new Paint();
            color1yellowpaint.setColor(getResources().getColor(R.color.yellowcolor1));
            color2yellowpaint=new Paint();
            color2yellowpaint.setColor(getResources().getColor(R.color.yellowcolor2));
            color3yellowpaint=new Paint();
            color3yellowpaint.setColor(getResources().getColor(R.color.yellowcolor3));
            color1pinkpaint=new Paint();
            color1pinkpaint.setColor(getResources().getColor(R.color.pinkcolor1));
            color2pinkpaint=new Paint();
            color2pinkpaint.setColor(getResources().getColor(R.color.pinkcolor2));
            color3pinkpaint=new Paint();
            color3pinkpaint.setColor(getResources().getColor(R.color.pinkcolor3));
            color1greenpaint=new Paint();
            color1greenpaint.setColor(getResources().getColor(R.color.greencolor1));
            color2greenpaint=new Paint();
            color2greenpaint.setColor(getResources().getColor(R.color.greencolor2));
            color3greenpaint=new Paint();
            color3greenpaint.setColor(getResources().getColor(R.color.greencolor3));
            color1brownpaint=new Paint();
            color1brownpaint.setColor(getResources().getColor(R.color.browncolor1));
            color2brownpaint=new Paint();
            color2brownpaint.setColor(getResources().getColor(R.color.browncolor2));
            color3brownpaint=new Paint();
            color3brownpaint.setColor(getResources().getColor(R.color.browncolor3));
            canvas=new Canvas();
            path = new Path();
        }
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        initialiseGrid(mGame,canvas);
    }

    public void setmForm(int[][] mGame) {
        this.mGame = mGame;
    }
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        numWidth=right-left;
        numHeight=bottom-top;
    }

    private void drawBrick(int x, int y, Canvas canvas,Paint mPaint1, Paint mPaint2,Paint mPaint3)
    {
        if(numHeight>numWidth)
        {
            int sizeForm=(numWidth/4);
            canvas.drawRect(x*sizeForm,y*sizeForm,sizeForm*(x+1),(y+1)*sizeForm,mPaint1);
            drawTriangle(x,y,sizeForm,canvas,mPaint3);
            float pourcentageMoveFromSquare = (sizeForm*10) / 100;
            canvas.drawRect(((x*sizeForm)+pourcentageMoveFromSquare),((y*sizeForm)+pourcentageMoveFromSquare),((sizeForm*(x+1))-pourcentageMoveFromSquare),(((y+1)*sizeForm)-pourcentageMoveFromSquare),mPaint2);
        }
        else
        {
            int sizeForm=numHeight/4;
            canvas.drawRect(x*sizeForm,y*sizeForm,sizeForm*(x+1),(y+1)*sizeForm,mPaint1);
            drawTriangle(x,y,sizeForm,canvas,mPaint3);
            float pourcentageMoveFromSquare = (sizeForm*10) / 100;
            canvas.drawRect(((x*sizeForm)+pourcentageMoveFromSquare),((y*sizeForm)+pourcentageMoveFromSquare),((sizeForm*(x+1))-pourcentageMoveFromSquare),(((y+1)*sizeForm)-pourcentageMoveFromSquare),mPaint2);
        }
    }
    private void drawTriangle(int x, int y,int size, Canvas canvas,Paint color3paint)
    {
        Path triangle = new Path();
        Point a,b,c;
        int pourcentageMoveFromSquare =(int)(size*3.33/100);
        a = new Point((x*size)+pourcentageMoveFromSquare,(y*size)+pourcentageMoveFromSquare);
        b = new Point((x*size)+pourcentageMoveFromSquare, ((y+1)*size)-pourcentageMoveFromSquare);
        c = new Point(((x+1)*size)-pourcentageMoveFromSquare,(y*size)+pourcentageMoveFromSquare);
        triangle.moveTo((x*size)+pourcentageMoveFromSquare,(y*size)+pourcentageMoveFromSquare);
        triangle.lineTo(b.x, b.y);
        triangle.lineTo(c.x, c.y);
        triangle.lineTo(a.x, a.y);
        triangle.close();
        canvas.drawPath(triangle, color3paint);
    }

   public void initialiseGrid (int[][] mForm, Canvas canvas)
    {
        ArrayList<Integer> temp = new ArrayList<>();
        for(int i=0;i<mForm.length;i++)//Colonnes
            for(int j=0;j<mForm[i].length;j++)//LIGNES
            {
                temp.add(mForm[i][j]);
                int numGrid = mForm[i][j];
                switch (numGrid)
                {
                    case 0:
                        drawBrick(i,j,canvas,color1transppaint,color2transppaint,color3transppaint);
                        break;
                    case 1:
                        drawBrick(i,j,canvas,color1redpaint,color2redpaint,color3redpaint);
                        break;
                    case 2:
                        drawBrick(i,j,canvas,color1bluelightpaint,color2bluelightpaint,color3bluelightpaint);
                        break;
                    case 3:
                        drawBrick(i,j,canvas,color1bluepaint,color2bluepaint,color3bluepaint);
                        break;
                    case 4:
                        drawBrick(i,j,canvas,color1yellowpaint,color2yellowpaint,color3yellowpaint);
                        break;
                    case 5:
                        drawBrick(i,j,canvas,color1pinkpaint,color2pinkpaint,color3pinkpaint);
                        break;
                    case 6:
                        drawBrick(i,j,canvas,color1greenpaint,color2greenpaint,color3greenpaint);
                        break;
                    case 7:
                        drawBrick(i,j,canvas,color1brownpaint,color2brownpaint,color3brownpaint);
                    break;
                }
                //ajoute la valeur dans un tableau temporaire
                temp.add(mForm[i][j]);
            }
            temp.clear();
        }
    }
*/
