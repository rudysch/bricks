package com.rsc.bricks.View;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import com.rsc.bricks.R;

/**
 * Created by Rudy Schoepfer on 25/02/2017.
 */

public class Bloc_View extends View {
    Paint color1paint,color2paint, color3paint;
    float left,right,top,bottom;
    Path path = new Path();

    public Bloc_View(Context context) {
        super(context);
        init(context,null,0);
    }
    public Bloc_View(Context context,int form) {
        super(context);
        init(context,null,form);
    }
    public Bloc_View(Context context, AttributeSet attrs,int form) {
        super(context, attrs);
        init(context,attrs,form);
    }
    public Bloc_View(Context context, AttributeSet attrs, int defStyleAttr,int form) {
        super(context, attrs, defStyleAttr);
        init(context,attrs,form);
    }
    private void init(Context context,AttributeSet attrs,int form)
    {
        if (context != null /*&& attrs != null*/)
        {
            switch(form)
            {
                case 0:
                    setColorForForm(getResources().getColor(R.color.transpcolor1),getResources().getColor(R.color.transpcolor2)
                    ,getResources().getColor(R.color.transpcolor3));
                    break;
                case 1:
                    setColorForForm(getResources().getColor(R.color.bluecolor1),getResources().getColor(R.color.bluecolor2)
                            ,getResources().getColor(R.color.bluecolor3));
                    break;
                case 2:
                    setColorForForm(getResources().getColor(R.color.bluelightcolor1),getResources().getColor(R.color.bluelightcolor2)
                            ,getResources().getColor(R.color.bluelightcolor3));
                    break;
                case 3:
                    setColorForForm(getResources().getColor(R.color.browncolor1),getResources().getColor(R.color.browncolor2)
                            ,getResources().getColor(R.color.browncolor3));
                    break;
                case 4:
                    setColorForForm(getResources().getColor(R.color.greencolor1),getResources().getColor(R.color.greencolor2)
                            ,getResources().getColor(R.color.greencolor3));
                    break;
                case 5:
                    setColorForForm(getResources().getColor(R.color.pinkcolor1),getResources().getColor(R.color.pinkcolor2)
                            ,getResources().getColor(R.color.pinkcolor3));
                    break;
                case 6:
                    setColorForForm(getResources().getColor(R.color.redcolor1),getResources().getColor(R.color.redcolor2)
                            ,getResources().getColor(R.color.redcolor3));
                    break;
                case 7:
                    setColorForForm(getResources().getColor(R.color.yellowcolor1),getResources().getColor(R.color.yellowcolor2)
                            ,getResources().getColor(R.color.yellowcolor2));
                    break;
            }
        }
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(0,top,right,bottom,color1paint);
        drawTriangle(bottom,right,left,canvas);
        drawSecondSquare(path,canvas,left,top,right,bottom);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        this.left = left;
        this.right = right;
        this.top = top;
        this.bottom = bottom;
    }
    private void drawTriangle(float bottom, float right,float left,Canvas canvas)
    {
        float pourcentageMoveFromSquare =(float)(right*3.33/100);
        path.moveTo(pourcentageMoveFromSquare,pourcentageMoveFromSquare);
        path.lineTo(pourcentageMoveFromSquare, bottom-pourcentageMoveFromSquare);
        path.lineTo(right-(left+pourcentageMoveFromSquare), pourcentageMoveFromSquare);
        path.close();
        canvas.drawPath(path, color2paint);
    }

    private void setColorForForm(int color1, int color2, int color3) {
        color1paint.setColor(color1);
        color2paint.setColor(color2);
        color3paint.setColor(color3);

    }
    private void drawSecondSquare(Path path, Canvas canvas,float left,float top, float right,float bottom) {
        float pourcentageMoveFromSquare = right * 10 / 100;
        path.moveTo(pourcentageMoveFromSquare, pourcentageMoveFromSquare);
        canvas.drawRect(pourcentageMoveFromSquare,pourcentageMoveFromSquare,right-pourcentageMoveFromSquare,bottom-pourcentageMoveFromSquare,color3paint);
        path.close();
    }
}
