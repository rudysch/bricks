package com.rsc.bricks.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.rsc.bricks.R;

/**
 * Created by Rudy Schoepfer on 24/04/2017.
 */

public class BroadcastBattery extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // affichage d'un message si la batterie est presque vide :
        Toast.makeText(context, context.getString(R.string.message_batterie), Toast.LENGTH_LONG).show();
    }
}
