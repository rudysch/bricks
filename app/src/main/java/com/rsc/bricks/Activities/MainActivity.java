package com.rsc.bricks.Activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.rsc.bricks.R;

public class MainActivity extends AppCompatActivity {

    //Buttons
    Button mEasyButton=null;
    Button mMediumButton=null;
    Button mDifficultButton=null;

    //Titre
    TextView mLevelTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mEasyButton = (Button)findViewById(R.id.buttonEasy);
        mMediumButton = (Button)findViewById(R.id.buttonMedium);
        mDifficultButton = (Button)findViewById(R.id.buttonDifficult);

        //Set la police
        Typeface fontRobotoMedium = Typeface.createFromAsset(getAssets(),"font/Roboto-Medium.ttf");
        Typeface fontRobotoBlack = Typeface.createFromAsset(getAssets(),"font/Roboto-Black.ttf");

        mEasyButton.setTypeface(fontRobotoMedium);
        mMediumButton.setTypeface(fontRobotoMedium);
        mDifficultButton.setTypeface(fontRobotoMedium);

        //mLevelTitle.setTypeface(fontRobotoBlack);
        //Set click sur bouton
        mEasyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,GameActivity.class);
                intent.putExtra("Level","Facile");
                startActivity(intent);
            }
        });
        mMediumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,GameActivity.class);
                intent.putExtra("Level","Moyen");
                startActivity(intent);
            }
        });
        mDifficultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,GameActivity.class);
                intent.putExtra("Level","Difficile");
                startActivity(intent);
            }
        });
    }
}
