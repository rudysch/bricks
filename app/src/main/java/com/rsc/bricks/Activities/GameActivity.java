package com.rsc.bricks.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.rsc.bricks.Class.Forms;
import com.rsc.bricks.Class.Score;
import com.rsc.bricks.Events.OnSwipeTouchListener;
import com.rsc.bricks.R;
import com.rsc.bricks.View.Game_Surface_View;

public class GameActivity extends AppCompatActivity{
    ImageButton mMenu, mReplay, mPause;
    TextView mScore, mLevel;
    Game_Surface_View mSurface;
    //Next_View_Surface mNextView;


    int[][] mGame = resetGrid();
    Forms myForm;
    Score scoreNumber = myForm.setupScore;
    int levelNumb, valeurTimerSet;
    //Variable du thread
    Thread t = new Thread();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_surface_activity);
        mSurface = (Game_Surface_View) findViewById(R.id.mGameSurfaceView);
        //mNextView = (Next_View_Surface) findViewById(R.id.next_surface_view);
        mScore=(TextView) findViewById(R.id.textviewScore);
        mLevel=(TextView) findViewById(R.id.textviewLevel);
        //Set la police
        Typeface fontRobotoItalic = Typeface.createFromAsset(getAssets(),"font/Roboto-BlackItalic.ttf");
        mScore.setTypeface(fontRobotoItalic);
        mLevel.setTypeface(fontRobotoItalic);

        //Initialisation du Score et du niveau
        levelNumb=(scoreNumber.getMyScore()+100)/100;
        valeurTimerSet=2000-((levelNumb-1)*200);

        //Enregistrement du Score et du niveau dans un sharedpreference
        SharedPreferences preferences = getSharedPreferences("myresult",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("Score", scoreNumber.getMyScore());
        editor.putInt("level", levelNumb);
        editor.commit();

        mSurface.setmGame(mGame);
        t = new Thread() {
            @Override
            public void run() {
                try {
                    while ((Thread.currentThread() == t) && (!t.isInterrupted())) {
                        if(valeurTimerSet>200)
                        {
                            valeurTimerSet=valeurTimerSet-((levelNumb-1)*200);
                        }
                        else
                        {
                            valeurTimerSet=200;
                        }
                        Log.d("TAGThread",Integer.toString(valeurTimerSet));
                        Thread.sleep(valeurTimerSet);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                myForm.down(mGame);
                                mSurface.invalidate();
                                //Affichage des valeurs dans les textviews associÃ©s
                                mScore.setText("SCORE:"+Integer.toString(scoreNumber.getMyScore()));
                                if(scoreNumber.getMyScore()<=100) {
                                    mLevel.setText("NIVEAU:" + Integer.toString(levelNumb));
                                }
                                else
                                {
                                    mLevel.setText("NIVEAU:" + Integer.toString((scoreNumber.getMyScore()+100)/100));
                                }
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };
        t.start();

        mMenu = (ImageButton) findViewById(R.id.menuButton);
        mPause = (ImageButton) findViewById(R.id.pauseButton);
        mReplay = (ImageButton) findViewById(R.id.replayButton);

        mMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GameActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        mReplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetGrid();
            }
        });

        mSurface.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeLeft() {
                myForm.left_moove(mGame);
                mSurface.invalidate();
            }

            @Override
            public void onSwipeRight() {
                myForm.right_moove(mGame);
                mSurface.invalidate();
            }

            @Override
            public void onSingleTouch() {
                myForm.turn_Form(mGame);
                mSurface.invalidate();
            }

            @Override
            public void onSwipeBottom() {
                boolean stop= false;

                while(stop==false)
                {
                    if(myForm.conflit(mGame)==true){
                        myForm.down(mGame);
                        mSurface.invalidate();
                    }
                    else{
                        stop=true;
                    }
                }

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(t==null)
        {
            t.start();
        }
        //Message alerte Batterie faible (en-cours)
        /*Intent intent = new Intent();
        intent.setAction(".Receiver.BroadcastBattery");
        sendBroadcast(intent);*/
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (t != null) {
            t.interrupt();
            t = null;
        }
    }

    private int[][] resetGrid() {
        myForm=new Forms();
        int [][] newGrid = new int [11][21];

        for (int i=0;i<10;i++) {
            for (int j=0;j<20;j++) {
                newGrid [i][j]=0;
            }
        }

        for (int i=0;i<10;i++) {
            newGrid [i][20]=1;
        }
        myForm.create_Form();
        myForm.show_Form(newGrid);
        myForm.setupScore.setMyScore(0);
        return newGrid;
    }
}
