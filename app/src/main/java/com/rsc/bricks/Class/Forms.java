package com.rsc.bricks.Class;

import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

public class Forms{
    //CrÃ©ation de toutes les piÃ¨ces
    public int[][] Form1 = { {0,1,0,0,0,1,1,0,0,0,1,0,0,0,0,0},
            {0,0,0,0,0,0,1,1,0,1,1,0,0,0,0,0},
            {0,1,0,0,0,1,1,0,0,0,1,0,0,0,0,0},
            {0,0,0,0,0,0,1,1,0,1,1,0,0,0,0,0} };
    public int[][] Form2 = { {0,2,2,0,0,2,2,0,0,0,0,0,0,0,0,0},
            {0,2,2,0,0,2,2,0,0,0,0,0,0,0,0,0},
            {0,2,2,0,0,2,2,0,0,0,0,0,0,0,0,0},
            {0,2,2,0,0,2,2,0,0,0,0,0,0,0,0,0} };
    public int[][] Form3 = { {0,3,0,0,0,3,0,0,0,3,0,0,0,3,0,0},
            {3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0},
            {0,3,0,0,0,3,0,0,0,3,0,0,0,3,0,0},
            {3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0} };
    public int[][] Form4 = { {0,0,4,0,0,4,4,0,0,4,0,0,0,0,0,0},
            {0,0,0,0,0,4,4,0,0,0,4,4,0,0,0,0},
            {0,0,4,0,0,4,4,0,0,4,0,0,0,0,0,0},
            {0,0,0,0,0,4,4,0,0,0,4,4,0,0,0,0} };
    public int[][] Form5 = { {0,5,0,0,0,5,5,0,0,5,0,0,0,0,0,0},
            {0,0,0,0,0,0,5,0,0,5,5,5,0,0,0,0},
            {0,0,0,5,0,0,5,5,0,0,0,5,0,0,0,0},
            {0,5,5,5,0,0,5,0,0,0,0,0,0,0,0,0} };
    public int[][] Form6 = { {0,0,6,0,0,0,6,0,0,6,6,0,0,0,0,0},
            {0,0,0,0,0,6,6,6,0,0,0,6,0,0,0,0},
            {0,6,6,0,0,6,0,0,0,6,0,0,0,0,0,0},
            {0,0,0,0,0,6,0,0,0,6,6,6,0,0,0,0} };
    public int[][] Form7 = { {0,7,0,0,0,7,0,0,0,7,7,0,0,0,0,0},
            {0,0,0,0,0,0,0,7,0,7,7,7,0,0,0,0},
            {0,7,7,0,0,0,7,0,0,0,7,0,0,0,0,0},
            {0,0,0,0,0,7,7,7,0,7,0,0,0,0,0,0} };
    int numero;
    int position = 0;
    int x = 4;
    int y = 0;
    int x1 = 0;
    int y1 = 0;
    public int [][] pieceCourante =  new int [4][16];
    ArrayList<Integer> allNumeroForm= new ArrayList<>();
    public Score setupScore = new Score();
    Random rand = new Random();

    boolean b = false;
    public Forms(){
        for (int i=0;i<4;i++) {
            for (int j=0;j<16;j++) {
                pieceCourante [i][j]=0;
            }
        }
    }
    public void create_Form() {
        /***************************************/
        // GÃ©nÃ©ration d'un nombre entre 1 et 4 pour choisir une forme de maniÃ¨re alÃ©atoire
        //nombre alÃ©atoire pour sÃ©lectionner la forme


        numero = rand.nextInt(7);
        for(int i=0;i<allNumeroForm.size();i++)
        {
            if(numero==allNumeroForm.get(i))
            {
                numero = 1 + ((int)(Math.random()*7));
            }
        }

        if(allNumeroForm.size()<7){
            allNumeroForm.add(numero);
        }
        else
        {
            allNumeroForm.clear();
        }
        if (numero < 1) numero = numero + 4;
        switch (numero) {
            //En fonction du numÃ©ro gÃ©nÃ©rÃ© on gÃ©nÃ¨re la piÃ¨ce
            case 7:   for (int i=0;i<4;i++) { for (int j=0;j<16;j++) {pieceCourante [i][j]=Form7 [i][j];  }}break;
            case 6:   for (int i=0;i<4;i++) { for (int j=0;j<16;j++) { pieceCourante [i][j]=Form6 [i][j]; }} break;
            case 5:   for (int i=0;i<4;i++) { for (int j=0;j<16;j++) { pieceCourante [i][j]=Form5 [i][j]; }} break;
            case 4:   for (int i=0;i<4;i++) { for (int j=0;j<16;j++) { pieceCourante [i][j]=Form4 [i][j]; }} break;
            case 3:   for (int i=0;i<4;i++) { for (int j=0;j<16;j++) { pieceCourante [i][j]=Form3 [i][j]; }} break;
            case 2:   for (int i=0;i<4;i++) { for (int j=0;j<16;j++) { pieceCourante [i][j]=Form2 [i][j]; }} break;
            case 1:   for (int i=0;i<4;i++) { for (int j=0;j<16;j++) { pieceCourante [i][j]=Form1 [i][j]; }} break;
        }
    }

    /******************GESTION DU SWIPE GAUCHE ET DROITE************************/
    //Gestion de l'Ã©vÃ¨nement - Swipe par la gauche
    public void left_moove(int[][] g) {
        //efface la forme
        deleteForm(g);

        //DÃ©crÃ©mente la position x pour dÃ©caler la forme
        x--;

        //Si jamais la forme entre en conflit alors on incrÃ©mente de nouveau la forme et on l'affiche
        if (conflit_left(g)){
            x++;
        }
        show_Form(g);
    }

    //Gestion de l'Ã©vÃ¨nement - Swipe par la droite
    public void right_moove(int[][] g) {
        //efface la forme
        deleteForm(g);

        //IncrÃ©mente la position x pour dÃ©caler la forme vers la droite
        x++;

        if (conflit_right(g)){
            x--;
        }
        show_Form(g);
    }

    /******************GESTION DU CONFLIT DU AU SWIPE GAUCHE ET DROITE************************/
    public boolean conflit_right(int[][] g)
    {
        boolean d = false;
        for (int m=0;m<16;m++) {
            // ContrÃ´le si la piÃ¨ce est remplie ou non
            if (pieceCourante[position][m] != 0) {
                int x1 = m % 4;  // colonne de la piÃ¨ce
                int y1 = m / 4;  // Ligne de la piÃ¨ce
                if ((x+x1) > 9){
                    return true;
                }
                else {
                    if (g[x+x1][y+y1] != 0){
                        return true;
                    }
                }
            }
        }
        return d;
    }
    public boolean conflit_left(int[][] g)
    {
        boolean d = false;
        for (int m=0;m<16;m++) {
            // ContrÃ´le si la piÃ¨ce est remplie ou non
            if (pieceCourante[position][m] != 0) {

                x1 = m % 4;  // colonne de la piÃ¨ce
                y1 = m / 4;  // Ligne de la piÃ¨ce

                if ((x+x1) < 0 )
                {
                    return true;
                }
                else
                {
                    if (g[x+x1][y+y1] != 0)
                    {
                        return true;
                    }
                }
            }
        }
        return d;
    }

    /******************GESTION DU CHANGEMENT DE LA FORME************************/
    public void turn_Form(int[][] g) {
        //on supprime temporairement la forme actuel
        deleteForm(g);
        //on sauvegarde l'ancienne position de la forme dans son tableau
        int old = position;
        //On choisi la forme suivante en incrÃ©mentant cette derniÃ¨re
        position ++;
        //Si la position est Ã©gale Ã  4 (fin du tableau Ã  3) alors on recommence au dÃ©but (0)
        if (position ==4)
        {
            position=0;
        }
        //Si lors du changement de forme il y a un conflit alors on revient Ã  la forme initiale
        if (conflit_turn(g))
        {
            position = old;
        }
        //Si il y a un conflit alors on reprend l'ancienne position
        if (conflit(g))
        {
            position = old;
        }
        show_Form(g);
    }
    /******************GESTION DU CONFLIT LORS DU CHANGEMENT DE FORME************************/
    public boolean conflit_turn(int[][] g)
    {
        boolean d = false;
        for (int m=0;m<16;m++) {
            // La case de la piÃ¨ce est-elle remplie ?
            if (pieceCourante[position][m] != 0) {

                x1 = m % 4;  // colonne de la piÃ¨ce
                y1 = m / 4;  // Ligne de la piÃ¨ce

                if ((x+x1) > 9 )
                {
                    return true;
                }

                if ((x+x1) < 0 )
                {
                    return true;
                }
                else {
                    if (g[x+x1][y+y1] != 0)
                    {
                        return true;
                    }
                }
            }
        }
        return d;
    }

    /******************GESTION DE L'AFFICHAGE DE LA FORME************************/
    public void show_Form(int[][] g) {
        for (int m = 0; m < 4; m++) {
            //Si le x est entre 0 et 9(colonnes)
            if (((x + m) < 10) && ((x + m) >= 0) && (g[x + m][y] == 0) && (pieceCourante[position][m] != 0)) {
                g[x + m][y] = pieceCourante[position][m];
            }
        }
        //Si le ligne(0)+1 est entre 0 et 20 (lignes) soit dans la grille
        if ((y + 1) < 21) {
            for (int m = 0; m < 4; m++) {
                if (((x + m) < 10) && ((x + m) >= 0) && (g[x + m][y + 1] == 0) && (pieceCourante[position][m + 4] != 0)) {
                    g[x + m][y + 1] = pieceCourante[position][m + 4];
                }
            }

        }
        //Si le ligne(0)+2 est entre 0 et 20 (lignes) soit dans la grille
        if ((y + 2) < 21) {
            for (int m = 0; m < 4; m++){
                if (((x + m) < 10) && ((x + m) >= 0) && (g[x + m][y + 2] == 0) && (pieceCourante[position][m + 8] != 0))
                {
                    g[x + m][y + 2] = pieceCourante[position][m + 8];
                }
            }
        }

        //Si le ligne(0)+3 est entre 0 et 20 (lignes) soit dans la grille
        if ((y + 3) < 21){
            for (int m = 0; m < 4; m++)
            {
                if (((x + m) < 10) && ((x + m) >= 0) && (g[x + m][y + 3] == 0) && (pieceCourante[position][m + 12] != 0))
                {
                    g[x + m][y + 3] = pieceCourante[position][m + 12];
                }
            }
        }
    }
    public boolean conflit(int[][] g)
    {
        boolean c = false;

        for (int m=0;m<4;m++) {
            if (((x+m)<10) && (y < 21) && ((x+m)>=0)) {
                if ((g[x + m][y] != 0) && (pieceCourante[position][m] != 0)) {
                    return true;
                }
            }
        }
        for (int m=0;m<4;m++) {
            if (((x+m)<10) && (y < 20) && ((x+m)>=0)) {
                if ((g[x + m][y + 1] != 0) && (pieceCourante[position][m + 4] != 0)) {
                    return true;
                }
            }
        }
        for (int m=0;m<4;m++) {
            if (((x+m)<10) && (y < 19) && ((x+m)>=0)){
                if ((g[x+m][y+2]!=0) && (pieceCourante[position][m+8]!=0)){
                    return true;
                }
            }
        }
        for (int m=0;m<4;m++) {
            if (((x+m)<10) && (y < 18) && ((x+m)>=0)){
                if ((g[x+m][y+3]!=0) && (pieceCourante[position][m+12]!=0)){
                    return true;
                }
            }
        }
        return c;
    }
    /******************GESTION DE LA SUPPRESSION DE LIGNE SI ELLE EST PLEINE************************/
    public void check_line(int[][] g) {
        int lineDelete=0;
        //Pour chaque ligne
        for (int m=0;m<20;m++) {
            b=false;
            //Controle des colonnes si une cellule est vide alors la ligne n'est pas pleine
            for (int n=0;n<10;n++)
            {
                if (g[n][m] == 0) b=true;
            }
            //AprÃ¨s le premier controle si b est toujours false, cela veut dire qu'une ligne est pleine
            if (b==false) {
                lineDelete++;
                //On initialise la derniÃ¨re ligne Ã  vide
                for (int i=0;i<10;i++)
                {
                    g[i][0]=0;
                }
                //On parcours le tableau de bas en haut pour dÃ©caler les Ã©lÃ©ments vers le bas
                for (int t=m;t>0;t--)/*Ligne*/ {
                    for (int o=0;o<10;o++)/*Colonne*/{
                        //le nouveau block est Ã©gale Ã  la piÃ¨ce du dessus
                        g[o][t]=g[o][t-1];
                    }
                }
            }
        }
        Log.d("TAGLineDeleted",Integer.toString(lineDelete));
        if(lineDelete!=0)
        {
            setupScore.setMyScore(setupScore.getMyScore()+(lineDelete*10));
        }
        Log.d("TAGNewScoreSet",Integer.toString(setupScore.getMyScore()));

    }
    public void deleteForm(int[][] g) {

        for (int m=0;m<4;m++) if (pieceCourante[position][m]!=0) g[x+m][y] = 0;
        for (int m=0;m<4;m++) if (pieceCourante[position][m+4]!=0) g[x+m][y+1] = 0;
        for (int m=0;m<4;m++) if (pieceCourante[position][m+8]!=0) g[x+m][y+2] = 0;
        for (int m=0;m<4;m++) if (pieceCourante[position][m+12]!=0) g[x+m][y+3] = 0;

    }
    public void loose(int[][] g)
    {
        for (int m=0;m<16;m++) {
            // La case de la pièce est-elle remplie ?
            if (pieceCourante[position][m] != 0) {

                x1 = m % 4;  // colonne de la pièce
                y1 = m / 4;  // Ligne de la pièce

                if (g[x+x1][y+y1] != 0) {
                    //Remise à zéro de la grille
                    for (int i=0;i<10;i++) {
                        for (int j=0;j<20;j++) {
                            g[i][j]=0;
                        }
                    }
                    setupScore.setMyScore(0);
                }
            }

        }
    }
    public void down(int[][] g)
    {
        deleteForm(g);

        y++;
        //La forme descend jusqu'Ã  un conflit et si jamais elle arrive en bas alors elle s'arrÃ¨te et en gÃ©nÃ¨re une nouvelle
        // sinon elle montre juste la forme. Une fois la forme arretÃ© on check les lignes savoir s'il est nÃ©cessaire d'en supprimÃ©.
        //CrÃ©ation d'une forme et check si on est pas en haut de la grille pour mettre fin au jeu
        if (conflit(g)) {
            y --;
            show_Form(g);
            check_line(g);
            y = 0;
            x = 3;
            create_Form();
            loose(g);
        } else show_Form(g);
    }
}
