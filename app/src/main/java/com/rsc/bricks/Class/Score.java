package com.rsc.bricks.Class;

/**
 * Created by Rudy Schoepfer on 25/04/2017.
 */

public class Score {
    int myScore =0;
    public Score(){}
    public Score(int myscore){
        this.myScore=myscore;
    }

    public int getMyScore() {
        return myScore;
    }

    public void setMyScore(int myScore) {
        this.myScore = myScore;
    }
}
